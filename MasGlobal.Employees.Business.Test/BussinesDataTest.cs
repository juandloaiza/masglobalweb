﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MasGlobal.Employees.Data;
using MasGlobal.Employees.Data.Interfaz;
using Unity;
using MasGlobal.Employees.Business.Interfaz;
using MasGlobal.Employees.Business.Implementation;

namespace MasGlobal.Employees.Business.Test
{
    [TestClass]
    public class BussinesDataTest
    {
        private UnityContainer unityContainer;
        public BussinesDataTest()
        {
            unityContainer = new UnityContainer();
            unityContainer.RegisterType<IEmployeeBO, EmployeeBO>();
        }
        [TestMethod]
        public void EmployeesInfo_Success_Test()
        {
            var emp = unityContainer.Resolve<IEmployeeBO>();
            var result = emp.GetAllEmployees();
            Assert.AreEqual(result[0].Name.ToLower(), "juan");
            Assert.AreEqual(result[1].Name.ToLower(), "sebastian");
        }

        [TestMethod]
        public void EmployeeInfo_Success_Test()
        {
            var emp = unityContainer.Resolve<IEmployeeBO>();
            var result = emp.GetEmployee(1);
            Assert.AreEqual(result[0].Name.ToLower(), "juan");
        }
    }
}
