﻿using System.Collections.Generic;

namespace MasGlobal.Employees.Data.Interfaz
{
    public interface IEmployeeDAL
    {
        string GetEmployeeList(string apiUrl);
        
    }
}
