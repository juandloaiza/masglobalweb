﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
namespace MasGlobal.Employees.Data
{
    public static class BussinessHelper
    {
        public static string GetConfigKey(string key)
        {
            var settingValue = string.Empty;
            try
            {
                settingValue = ConfigurationSettings.AppSettings[key];
            }
            catch(Exception ex)
            {
                //Write on log
                throw new Exception(string.Format("Web config error: {0}", ex.Message), ex.InnerException);
            }
            return settingValue;
        }

        public static List<T> GetObjectList<T>(string json)
        {
            return JsonConvert.DeserializeObject<List<T>>(json);
        }


    }
}
