﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MasGlobal.Employees.Web.Startup))]
namespace MasGlobal.Employees.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
