﻿using MasGlobal.Employees.Business.Interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasGlobal.Employees.Business.Dtos;
using Unity;
using MasGlobal.Employees.Data.Interfaz;
using MasGlobal.Employees.Data;

namespace MasGlobal.Employees.Business.Implementation
{
    public class EmployeeBO : IEmployeeBO
    {
        private UnityContainer unityContainer;
        public EmployeeBO()
        {
            // Declare un contenedor Unity
            unityContainer = new UnityContainer();
            // Registramos IEmployeeDAL para que cuando se detecte la dependencia
            // proporcione una instancia de EmployeeDAL
            unityContainer.RegisterType<IEmployeeDAL, EmployeeDAL>();
        }
        public List<Employee> GetAllEmployees()
        {
            var employees = new List<Employee>();
            var da = unityContainer.Resolve<IEmployeeDAL>();
            var apuUri = BussinessHelper.GetConfigKey("ApiUrl");
            var employeesJson = da.GetEmployeeList(apuUri);
            employees = BussinessHelper.GetObjectList<Employee>(employeesJson);
            return employees;
        }

        public List<Employee> GetEmployee(int id)
        {
            var employees = new List<Employee>();
            var da = unityContainer.Resolve<IEmployeeDAL>();
            var apuUri = BussinessHelper.GetConfigKey("ApiUrl");
            var employeesJson = da.GetEmployeeList(apuUri);
            employees = BussinessHelper.GetObjectList<Employee>(employeesJson);
            return employees.Where(i => i.Id.Equals(id)).ToList();
        }

        
    }
}
