﻿using MasGlobal.Employees.Business.Dtos;
using MasGlobal.Employees.Business.Implementation;
using MasGlobal.Employees.Business.Interfaz;
using System.Collections.Generic;
using System.Web.Http;
using Unity;

namespace MasGlobal.Employees.Web.Controllers
{
    //GET
    //[RoutePrefix("/api/Employees")]
    public class EmployeeController : ApiController
    {
        private UnityContainer unityContainer;
        public EmployeeController()
        {
            // Declare UnityContainer
            unityContainer = new UnityContainer();
            // Register IDataAccess to get an instance of DataAccess
            unityContainer.RegisterType<IEmployeeBO, EmployeeBO>();

        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public IEnumerable<Employee> GetAllEmployees(int? id)
        {
            var bo = unityContainer.Resolve<IEmployeeBO>();
            if(id == null)
                return bo.GetAllEmployees();
            return bo.GetEmployee((int)id);
        }
    }
}
