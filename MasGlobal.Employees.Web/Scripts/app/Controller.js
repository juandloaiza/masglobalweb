﻿app.controller('myController', function ($scope, $http) {
    $scope.searchEmployee = function () {
        
        $http.get('api/Employee/GetAllEmployees', { params: { id: $scope.idEmployee } } ).then(function (response) { 
            $scope.employees = response.data;
        });
    };
    $scope.sortColumn = "ContractTypeName";
});