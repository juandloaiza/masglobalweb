﻿using MasGlobal.Employees.Business.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasGlobal.Employees.Business.Interfaz
{
    public interface IEmployeeBO
    {
        List<Employee> GetAllEmployees();

        List<Employee> GetEmployee(int id);
    }
}
