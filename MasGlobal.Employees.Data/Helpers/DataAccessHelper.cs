﻿using System.IO;
using System.Net;

namespace MasGlobal.Employees.Business.Helpers
{
    public static class DataAccessHelper
    {
        public static string GetObjectList(string apiUrl)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();                
            }
        }
    }
}
